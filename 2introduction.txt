title: introduction
----
track: 01
----
post:

earlier this year, my friend @CBLGH shared their awesome project Datradio.  Datradio is a way to share music and mixtapes in a p2p way--a music player for the Chorus.  It's in an explicitly early state, an expression of intent and not a final project.  But this early vision excited me soooo much, and I wanted to help however I can.  Alex felt confident on the techinical side, but wanted help envisioning where datradio could go, the different shapes p2p music sharing could take.  And so I offered my help through this zine.
~~*

This zine describes different visions for music in the chorus, and ways we can design and develop software in this space.  If you're reading this zine, you likely already know what the Chorus is--but just to be safe:  the Chorus describes a world of person-to-person technology that is decentralized and loving, and that seeks to empower humans and their communities, instead of just controlling and monetizing data us humans generate.  It's  exemplified through the protocols Secure Scuttlebutt and Dat, and programs like patchwork/patchbay, the beaker Browser, and cabal.  The Chorus emerged from the web, but is not of the web--it is the place that comes after; it is an optimistic solarpunk future that we build together. 

~~*    

I am not writing this from a place of strategy or advising--like, this isn't a checklist of "things that must be done".  Who am I to say what music in the chorus should look like?  Who is anyone to say?  Tech folks have a bad history of prescribing how the world _should_ work, of building perfect systems that will fix whatever ill they've devised and then they  just need to convince the 'non-technical' person to switch over to their genius idea. It's a condescending and oppresive style that is annoying to hear and impossible to realize.

~~*

Instead, I offer a set of personal essays about a future that inspires me and will hopefully inspire you.  I don't know what shape our technology will take--I just know that we can work together to create a future that is romantic and just, and I am excited for the beautiful forms that arise on our path to get there.

I am presenting to you the big prairie of my heart, each future feeling  its own blade of grass, swaying with the others.
