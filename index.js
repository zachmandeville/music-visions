// Bring in our outdoor modules
const choo = require('choo')

// Bring in our indoor modules
const cover = require('./cassette/components/cover')
const song = require('./cassette/components/song')
const songbook = require('./cassette/components/songbook')

// Initialize choo
const app = choo()

// if in dev mode, use choo-devtools, if in production, use the service worker
if (process.env.NODE_ENV !== 'production') {
  app.use(require('choo-devtools')())
} else {
  app.use(require('choo-service-worker')())
}

app.use(require('./tape/stores/songs'))
app.use(require('./tape/stores/songNavigation'))

app.mount('body')
app.route('/', cover)
app.route('#songbook', songbook)
app.route('#songbook/:song', song)
