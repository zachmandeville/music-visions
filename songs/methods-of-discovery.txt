title: discovery
----
track: 06
----
post:

Discovery is important,  otherwise you'd just have a personal music player on a self-hosted cloud--which is not really exciting to me.  Then how does someone discover other people's datradio archives and datradio collections?

~~*

If we see the work we are doing as a wholly new thing, the beginning of the Chrous instead of the continuation of the web, then we can seek inspiration in places beyond just how current web apps do it.  This is a great boon, because we knew all _kinds_ of ways to discover music before Spotify's dollar-weighted algorithms. 
~~*

How did discovery work before?  A few examples:

~~*

A friend gives you a recommendation.  You don't know how _they_ found the music, but their taste is excellent.

~~*

An intriguing bandname on the t-shirt of a crush.
~~*

A mixtape given to you personally by a crushee.
~~*

A record store-, picking out newmusic based on the name on the CD spine, or the art on the vinyl sleeve, or because it's on the 'Kelly' row of the record store's recommendation shelf, and you know Kelly is the employee with the best taste.

~~*

A diy venue that became a second home to you and your scene.  Going to a random show there just cos it's friday, and you trust them, and then you hear your three new favorite bands all on the same night.
~~*

Long car rides and sharing control of the stereo.
~~*

A message board for a band/record label/city/genre.  Asking others on this same board what they're listening to.
~~*

Browsing the shelf of tapes in a friend's room.

~~*
Getting a sampler from a record label, or checking out all the samples on the 'Artists' page of a record label you love.
~~*
An mp3 blog.

~~*

A fanzine
~~*

Chance.
~~*

How can we augment these existing experiences with our technology?  How can we create similar, but new, decentralized discoveries?
