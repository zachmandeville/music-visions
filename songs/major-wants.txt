title: major wants
----
track: 03
----
post:

This is what I'm trying to say.  These are my hopes, my overall suggestions.
~~*

Don't code a platform that appeals to the widest amount of people.  Build a tool that helps one person reach out to one other person. 

~~*

Bring back regionalism.  Bring back the wonder of local scenes
~~*

Bring back physicality.  Make that a part of our design.  Code with our entire bodies, with our entire bodies in mind.
~~*

Bring back the vulnerable, but powerful abilities of a mixtape.  The vulnerable, powerful ability of all art.  Give people another way to put their entire heart and desire into a thing to share with whoever their most important person is.  Don't try to change the world, try to help people get laid. 

~~*

Bring forth the magic already inherent in all of this.
~~*

Make people less sad.
~~*
----
